# Getting Started with Full Text Search in Postgres

At City Innovation Labs we are focused on applying the methodologies of lean start up to help innovation teams bring their ideas to life.  One of the core principles of lean start up is start small and make small iterations based on user feedback.  Search is a common feature in many products today.  The time to build and deploy a fully realized search feature can take weeks.  If your product already uses Postgres the time to build a search feature can go from weeks to hours by using Postgres’s built in full text search features.   That will ultimately help you launch your product faster.  Once search has been validated as a feature that users want to work really well, then you can take the time to build the fully realized search feature using proven technologies like [Elastic Search](https://www.elastic.co/) or [Algolia](https://www.algolia.com).  This post focuses on how to use the full text search (FTS) features of Postgres to get your MVP out as soon as possible.

Most relational databases offer very simple text search
functions like `LIKE`, `ILIKE`, `SIMILAR` `~`,  `~*` and
`regexp_matches`. However there are some limitations to those
functions that the full text search functionality overcomes.
Postgres supports stemming words (ie race and racing will
have a common stem), removing stop words (words that mean
nothing when you are searching, ie, the, a, to), and ranking.
There are other cool features that are worth checking out in the
[official documentation](https://www.postgresql.org/docs/10/textsearch-intro.html).

This post will use the dataset from the [Project
Gutenburg](https://www.gutenberg.org/) index as an example
database to search.  By the end of this we'll be able to look up books by title, description, author, and category.

Project Gutenburg offers its index available as a pile of [RDF/
XML files](http://www.gutenberg.org/wiki/Gutenberg:Feeds).  I
wrote a script to take that file and dump it into a PostgreSQL
database.  You can download the Postgres dump
[here](https://bitbucket.org/lumissolutions/blog-posts/downloads/dbexport.pgsql.zip).

# Simple Search

## Preparing the document

This example will have a few tables that'll be used for
searching.  Here's what the schema looks like:

```
CREATE TABLE books(
  id TEXT,
  title TEXT,
  alternative TEXT,
  language TEXT,
  table_of_contents TEXT,
  issued DATE,
  downloads INT,
  PRIMARY KEY (id)
);

CREATE TABLE creators(
  id TEXT,
  name TEXT,
  webpage TEXT,
  death_date TEXT,
  birth_date TEXT,
  PRIMARY KEY(id)
);

CREATE TABLE books_creators (
  book_id TEXT REFERENCES books(id),
  creator_id TEXT REFERENCES creators(id),
  PRIMARY KEY (book_id, creator_id)
);

CREATE TABLE subjects(
  id SERIAL,
  value TEXT,
  kind TEXT,
  PRIMARY KEY(id)
);

CREATE TABLE books_subjects (
  book_id TEXT REFERENCES books(id),
  subject_id INT REFERENCES subjects(id),
  PRIMARY KEY (book_id, subject_id)
);

CREATE TABLE files(
  extant TEXT,
  mime_type TEXT,
  uri TEXT,
  modified TIMESTAMP,
  book_id TEXT REFERENCES books(id),
  PRIMARY KEY(uri)
);
```

Not all fields on the tables will be used for searching - some
will be used for informational purposes.  The files table is
used to link to the files hosted by Project Gutenburg and won't
be used in the search.  The first part of the search is to build
up a document that can be indexed and search.  This document
will include everything that a user may want to search on.

Here's an initial query that pulls together the data to get all
the fields needed for the search.  Since this is a test search,
it's limited to 10 rows.

```
SELECT books.id,
COALESCE(books.title, '') || ' ' ||
COALESCE(books.alternative, '') || ' ' ||
COALESCE(books.table_of_contents, '') || ' ' ||
COALESCE(creators.name, '') doc
FROM books
LEFT JOIN books_creators ON (books_creators.book_id = books.id)
LEFT JOIN creators ON (books_creators.creator_id = creators.id)
LIMIT 10;

      id      │                                         doc
──────────────┼─────────────────────────────────────────────────────────────────────────────────────
 ebooks/0     │
 ebooks/1     │ The Declaration of Independence of the United States of America   Jefferson, Thomas
 ebooks/10    │ The King James Version of the Bible
 ebooks/100   │ The Complete Works of William Shakespeare   Shakespeare, William
 ebooks/1000  │ La Divina Commedia di Dante: Complete   Dante Alighieri
 ebooks/10000 │ The Magna Carta   Anonymous
 ebooks/10001 │ Apocolocyntosis   Seneca, Lucius Annaeus
 ebooks/10002 │ The House on the Borderland   Hodgson, William Hope
 ebooks/10003 │ My First Years as a Frenchwoman, 1876-1879  When MacMahon was president -- Impressi…
              │…ons of the Assembly at Versailles -- M. Waddington as minister of public instructio…
              │…n -- The social side of a minister's wife -- A republican victory and a new ministr…
              │…y -- The exposition year -- The Berlin congress -- Gaieties at the Quai d'Orsay -- …
              │…M. Waddington as prime minister -- Parliament back in Paris -- Last days at the for…
              │…eign office. Waddington, Mary King
 ebooks/10004 │ The Warriors  Chords of awakening: the higher conquest -- Prelude: the call of Jesu…
              │…s -- Processional: the church of God -- The world-march: of kings; of prelates and …
              │…evangelists; of sages; of traders; of workers. Lindsay, Anna Robertson Brown
(10 rows)
```

Some of the fields may be null so the COALESCE function is used
to default null to an empty text value otherwise the entire
document would have null value.  The subjects table has some
interesting data that users may want to include in the search,
but we'll include that at a different time.

Now that the initial document is defined, it needs to be
preprocessed into a special vector that has the words stemmed,
the stop words removed and the words ranked. (We'll get to
ranking later in the article).  The data type is called a
`tsvector`.  The function that turns a document into a
`tsvector` is `to_tsvector`.

Here's the query updated:

```
SELECT books.id,
to_tsvector(COALESCE(books.title, '') || ' ' ||
            COALESCE(books.alternative, '') || ' ' ||
            COALESCE(books.table_of_contents, '') || ' ' ||
            COALESCE(creators.name, '')) doc
FROM books
LEFT JOIN books_creators ON (books_creators.book_id = books.id)
LEFT JOIN creators ON (books_creators.creator_id = creators.id)
LIMIT 10;

      id      │                                         doc
──────────────┼─────────────────────────────────────────────────────────────────────────────────────
 ebooks/0     │
 ebooks/1     │ 'america':10 'declar':2 'independ':4 'jefferson':11 'state':8 'thoma':12 'unit':7
 ebooks/10    │ 'bibl':7 'jame':3 'king':2 'version':4
 ebooks/100   │ 'complet':2 'shakespear':6,7 'william':5,8 'work':3
 ebooks/1000  │ 'alighieri':8 'commedia':3 'complet':6 'dant':5,7 'di':4 'divina':2 'la':1
 ebooks/10000 │ 'anonym':4 'carta':3 'magna':2
 ebooks/10001 │ 'annaeus':4 'apocolocyntosi':1 'lucius':3 'seneca':2
 ebooks/10002 │ 'borderland':5 'hodgson':6 'hope':8 'hous':2 'william':7
 ebooks/10003 │ '-1879':8 '1876':7 'assembl':16 'back':59 'berlin':45 'congress':46 'd':51 'day':63…
              │… 'exposit':42 'first':2 'foreign':66 'frenchwoman':6 'gaieti':47 'impress':13 'inst…
              │…ruct':25 'king':70 'last':62 'm':19,53 'macmahon':10 'mari':69 'minist':22,31,57 'm…
              │…inistri':40 'new':39 'offic':67 'orsay':52 'pari':61 'parliament':58 'presid':12 'p…
              │…rime':56 'public':24 'quai':50 'republican':35 'side':28 'social':27 'versaill':18 …
              │…'victori':36 'waddington':20,54,68 'wife':33 'year':3,43
 ebooks/10004 │ 'anna':36 'awaken':5 'brown':38 'call':11 'chord':3 'church':16 'conquest':8 'evang…
              │…elist':28 'god':18 'higher':7 'jesus':13 'king':24 'lindsay':35 'march':22 'prelat'…
              │…:26 'prelud':9 'procession':14 'robertson':37 'sage':30 'trader':32 'warrior':2 'wo…
              │…rker':34 'world':21 'world-march':20
(10 rows)
```

In this output, the words are stemmed to a normalized token and the order of the word is to the right of it.  In
`ebooks/10003` the last token “year” has 3,43 by it because it
appears twice in the document.

Project Gutenburg has over 60,000 books.  Generating the vectors
on the fly for every search will be slow.  For this example a
materialized view will be utilized to store the vectors.  This
method was chosen because the project is more read heavy than
write heavy.  A few books per day are contributed.  They don't
have to show up in the index right away, so it's okay there's a
nightly job that pulls in new books and refreshes the view.

For projects that are write heavy, instead of using a
materialized view, you may want to consider using a separate
table that is either updated by the application or by triggers
whenever data is inserted, updated or deleted. You could also
use another column on the data you want preprocessed.  In this case I recommend keeping the search data in a separate table since it’s pulled together from different tables.

So here's the updated query to create the materialized view:

```
CREATE MATERIALIZED VIEW simple_search AS
SELECT books.id,
to_tsvector(COALESCE(books.title, '') || ' ' ||
            COALESCE(books.alternative, '') || ' ' ||
            COALESCE(books.table_of_contents, '') || ' ' ||
            COALESCE(creators.name, '')) doc
FROM books
LEFT JOIN books_creators ON (books_creators.book_id = books.id)
LEFT JOIN creators ON (books_creators.creator_id = creators.id);
```

Notice the limit has been removed so all the documents can
be preprocessed.

## Searching the document

Now the document has been preprocessed and put into a
materialized view for better searching.  Later we'll add an index
on it to speed it up even more.  Time to search the newly
created vectors.

First we'll search for documents that have the word `friend` in
them:

```
SELECT *
FROM simple_search
WHERE simple_search.doc @@ tsquery('friend')
LIMIT 10;

      id      │                                         doc
──────────────┼─────────────────────────────────────────────────────────────────────────────────────
 ebooks/5066  │ 'alic':74 'andrew':72 'aunt':18 'author':8 'bang':49 'boy':68 'brown':75 'cut':35 '…
              │…daughter':30,57 'daughter-in-law':29 'dean':12 'dyke':83 'e':20 'edith':63 'elizabe…
              │…th':40,58 'famili':3,80 'father':10 'freeman':22 'friend':77 'girl':39 'grandmoth':…
              │…24 'heaton':26,86 'henri':53,81 'howel':13 'jame':54 'john':47 'jordan':41 'kendric…
              │…k':48 'law':32,46 'maid':17 'mari':19,25,33,69,85 'marri':51,56 'mother':62 'novel'…
              │…:5 'old':16 'old-maid':15 'peggi':73 'phelp':60 'raymond':70 'school':38,67 'school…
              │…-boy':66 'school-girl':37 'shipman':71 'son':44,52 'son-in-law':43 'stewert':34 'st…
              │…uart':59 'twelv':7 'van':82 'vors':27,84 'whole':2 'wilkin':21 'william':11 'wyatt'…
              │…:64
 ebooks/3396  │ 'acquaint':11 'boston':2 'dean':14 'friend':9 'howel':12 'knew':5 'literari':1,8 'w…
              │…illiam':13
 ebooks/21189 │ 'aesop':1,10 'dress':9 'friend':5 'new':8 'old':4 'rhyme':3
 ebooks/17282 │ 'alphabet':2 'crane':6 'friend':5 'old':4 'walter':7
 ebooks/36565 │ 'dargan':11 'describ':4 'friend':10 'letter':7 'oliv':12 'poni':3 'tilford':13 'two…
              │…':6 'welsh':2
 ebooks/21323 │ 'church':3 'confess':9 'estim':13 'frank':8 'friend':12 'grenfel':17 'insid':16 'me…
              │…an':4 'sir':20 'thomason':19 'wilfr':18
 ebooks/39787 │ 'baro':26 'belov':6 'betteron':15 'emmuska':24 'episod':8 'friend':20 'honeywood':2…
              │…2 'john':21 'life':11 'majesti':2 'mr':13 'orczi':23,25 'thoma':14 'told':17 'well'…
              │…:5 'well-belov':4
 ebooks/46879 │ 'engin':7 'fire':12 'friend':4 'miner':2 'rais':9 'saveri':13 'thoma':14 'water':10
 ebooks/10635 │ '2':12 'adventur':22,47,80 'agrican':28 'angelica':24,49 'ariodant':56 'ariosto':38…
              │… 'armida':77 'astolfo':50 'believ':37 'boiardo':13 'clorinda':74 'critic':14,39,62 …
              │…'death':26 'enchant':83 'forest':84 'friend':31 'genius':20,45,68 'ginerva':58 'hun…
              │…t':85 'isabella':60 'italian':4 'journey':52 'leigh':86 'life':18,43,66 'live':7 'm…
              │…oon':55 'notic':15,40,63 'olindo':69 'part':32 'poet':5 'rinaldo':75 'saracen':30 '…
              │…second':34 'see':35 'sophronia':71 'stori':1 'suspicion':59 'tancr':72 'tasso':61 '…
              │…volum':11 'writer':10
 ebooks/3397  │ 'acquaint':8 'boston':3 'dean':11 'friend':6 'howel':9 'literari':5 'roundabout':1 …
              │…'william':10
(10 rows)
```

This is great, but this is not what users would want to see.  It
would be helpful if the title of the book and the author were
listed by as well to see which books have been found.  Below the query is updated to reflect that:

```
SELECT
books.title,
creators.name,
my_search.doc FROM
(SELECT *
  FROM simple_search
  WHERE simple_search.doc @@ phraseto_tsquery('friend')
  LIMIT 10) my_search
LEFT JOIN books ON (books.id = my_search.id)
LEFT JOIN books_creators ON books_creators.book_id = books.id
LEFT JOIN creators ON books_creators.creator_id = creators.id;

                                       title                                        │              name               │                                       doc
────────────────────────────────────────────────────────────────────────────────────┼─────────────────────────────────┼─────────────────────────────────────────────────────────────────────────────────
 The Whole Family: a Novel by Twelve Authors                                        │ Vorse, Mary Heaton              │ 'alic':74 'andrew':72 'aunt':18 'author':8 'bang':49 'boy':68 'brown':75 'cut':…
                                                                                    │                                 │…35 'daughter':30,57 'daughter-in-law':29 'dean':12 'dyke':83 'e':20 'edith':63 …
                                                                                    │                                 │…'elizabeth':40,58 'famili':3,80 'father':10 'freeman':22 'friend':77 'girl':39 …
                                                                                    │                                 │…'grandmoth':24 'heaton':26,86 'henri':53,81 'howel':13 'jame':54 'john':47 'jor…
                                                                                    │                                 │…dan':41 'kendrick':48 'law':32,46 'maid':17 'mari':19,25,33,69,85 'marri':51,56…
                                                                                    │                                 │… 'mother':62 'novel':5 'old':16 'old-maid':15 'peggi':73 'phelp':60 'raymond':7…
                                                                                    │                                 │…0 'school':38,67 'school-boy':66 'school-girl':37 'shipman':71 'son':44,52 'son…
                                                                                    │                                 │…-in-law':43 'stewert':34 'stuart':59 'twelv':7 'van':82 'vors':27,84 'whole':2 …
                                                                                    │                                 │…'wilkin':21 'william':11 'wyatt':64
 Literary Boston as I Knew It (from Literary Friends and Acquaintance)              │ Howells, William Dean           │ 'acquaint':11 'boston':2 'dean':14 'friend':9 'howel':12 'knew':5 'literari':1,…
                                                                                    │                                 │…8 'william':13
 Aesop, in Rhyme: Old Friends in a New Dress                                        │ Aesop                           │ 'aesop':1,10 'dress':9 'friend':5 'new':8 'old':4 'rhyme':3
 An Alphabet of Old Friends                                                         │ Crane, Walter                   │ 'alphabet':2 'crane':6 'friend':5 'old':4 'walter':7
 The Welsh Pony, Described in two letters to a friend                               │ Dargan, Olive Tilford           │ 'dargan':11 'describ':4 'friend':10 'letter':7 'oliv':12 'poni':3 'tilford':13 …
                                                                                    │                                 │…'two':6 'welsh':2
 What the Church Means to Me                                                       ↵│ Grenfell, Wilfred Thomason, Sir │ 'church':3 'confess':9 'estim':13 'frank':8 'friend':12 'grenfel':17 'insid':16…
 A Frank Confession and a Friendly Estimate by an Insider                           │                                 │… 'mean':4 'sir':20 'thomason':19 'wilfr':18
 His Majesty's Well-Beloved                                                        ↵│ Orczy, Emmuska Orczy, Baroness  │ 'baro':26 'belov':6 'betteron':15 'emmuska':24 'episod':8 'friend':20 'honeywoo…
 An Episode in the Life of Mr. Thomas Betteron as told by His Friend John Honeywood │                                 │…d':22 'john':21 'life':11 'majesti':2 'mr':13 'orczi':23,25 'thoma':14 'told':1…
                                                                                    │                                 │…7 'well':5 'well-belov':4
 The Miner's Friend; Or, An Engine to Raise Water by Fire                           │ Savery, Thomas                  │ 'engin':7 'fire':12 'friend':4 'miner':2 'rais':9 'saveri':13 'thoma':14 'water…
                                                                                    │                                 │…':10
 Stories from the Italian Poets: with Lives of the Writers, Volume 2                │ Hunt, Leigh                     │ '2':12 'adventur':22,47,80 'agrican':28 'angelica':24,49 'ariodant':56 'ariosto…
                                                                                    │                                 │…':38 'armida':77 'astolfo':50 'believ':37 'boiardo':13 'clorinda':74 'critic':1…
                                                                                    │                                 │…4,39,62 'death':26 'enchant':83 'forest':84 'friend':31 'genius':20,45,68 'gine…
                                                                                    │                                 │…rva':58 'hunt':85 'isabella':60 'italian':4 'journey':52 'leigh':86 'life':18,4…
                                                                                    │                                 │…3,66 'live':7 'moon':55 'notic':15,40,63 'olindo':69 'part':32 'poet':5 'rinald…
                                                                                    │                                 │…o':75 'saracen':30 'second':34 'see':35 'sophronia':71 'stori':1 'suspicion':59…
                                                                                    │                                 │… 'tancr':72 'tasso':61 'volum':11 'writer':10
 Roundabout to Boston (from Literary Friends and Acquaintance)                      │ Howells, William Dean           │ 'acquaint':8 'boston':3 'dean':11 'friend':6 'howel':9 'literari':5 'roundabout…
                                                                                    │                                 │…':1 'william':10
(10 rows)
```

That is more informative.  Notice that we select from a subquery.  That is so only search results are joined on not the entire search table.

Here are some useful functions for searching:

  - `to_tsquery`
  - `plainto_tsquery`
  - `phraseto_tsquery`

`to_tsquery` is the more powerful of the three because it
supports some pretty nifty operators:

  - & (AND)
  - | (OR)
  - ! (NOT)
  - <-> (FOLLOWED BY)

If we were exposing this to the web to be search, I would
recommend using `plainto_tsquery`.  It takes the words you pass
to is and searches for the document that contains all the
tokens.  `plainto_tsquery('war peace')` is equivalent to
`to_tsquery('war & peace')`.  It looks for documents that have
words similar to war and peace.

If I were building a typeahead though, I would want to use
to_tsquery.  I would remove any of the operators, parenthesis,
and other special characters from the search so the query
wouldn't throw an error.  Then I would join the words together
with the `&` operator and append to the last word `:*`.  Then
`christmas ca` would be turned into `christmas & ca:*`.

It's worth checking out the
[documentation](https://www.postgresql.org/docs/10/textsearch-controls.html#TEXTSEARCH-PARSING-QUERIES)
to see more example of these operators in action.

# Search Ranking

It's important that the search show the most relevant results at
the top.  This can be done with PostgreSQL's ranking functions.
The `ts_rank` function works by ranking results higher if the
tokens in the query vector occur more frequently in the document
vector.  Here's an example query:

```
SELECT
my_search.rank,
books.title,
creators.name,
my_search.doc,
query
FROM (SELECT
      simple_search.id,
      simple_search.doc,
      query,
      ts_rank(simple_search.doc, query) rank
      FROM simple_search, to_tsquery('christmas & ca:*') AS query
      WHERE query @@ simple_search.doc
      ORDER BY rank DESC
      LIMIT 50) my_search
LEFT JOIN books ON (books.id = my_search.id)
LEFT JOIN books_creators ON books_creators.book_id = books.id
LEFT JOIN creators ON books_creators.creator_id = creators.id
```

The documents say that ranking is expensive, however this query
returned in 50 ms for 30,000 documents.  That's even without
adding an index on the document as well.

Before moving on let's add two indexes on the search view.  The
first is on the id.  That'll allow the materialized view to be
updated concurrently.  The other will be on the document which
should speed up the searches.

`CREATE INDEX idx_fts_search ON simple_search USING gin(doc);`
`CREATE UNIQUE INDEX idx_unq_search ON simple_search (id);`

That sped up the search.  Now the results are returning in
about 10 ms.

## Weights

One thing that would help ranking results is if certain items in
the documents were weighted more than others.  For example, the
title of the book should be  more relevant than the table of
contents.  Also, there's the subjects table we could add to the
document to give some more text to the document to help with
searching.  However, that would be weighted the lowest of the
items related to the book.  If a person were search history, books with that in
the title would show up first, then books with that in the
subject would show up last.  How items are weighted in the document is very application specific.

PostgreSQL has 4 different categories of weights that can added to
the documents, A, B, C, and D.

Here's a query to create a ranked document as a materialized
view to search.


```
CREATE MATERIALIZED VIEW ranked_search AS
SELECT books.id,
setweight(to_tsvector(COALESCE(books.title, '') || ' ' || COALESCE(books.alternative, '')), 'A') ||
setweight(to_tsvector(COALESCE(creators.name, '')), 'B') ||
setweight(to_tsvector(COALESCE(books.table_of_contents, '')), 'C') ||
setweight(to_tsvector(COALESCE(subject_groups.subject_values, '')), 'D') AS doc
FROM books
LEFT JOIN books_creators ON (books_creators.book_id = books.id)
LEFT JOIN creators ON (books_creators.creator_id = creators.id)
LEFT JOIN (SELECT
			 books_subjects.book_id,
             string_agg(subjects.value , ' ') subject_values
             FROM subjects
             LEFT JOIN books_subjects ON (books_subjects.subject_id = subjects.id)
             GROUP BY books_subjects.book_id) subject_groups
          ON (subject_groups.book_id = books.id);
```

We'll also index it:
```
CREATE UNIQUE INDEX idx_unq_search_ranked ON ranked_search (id);
```
```
CREATE INDEX idx_fts_search ON ranked_search USING gin(doc);
```

This can be searched liked the last view, except the view names
need to be changed. See below:

```
SELECT
my_search.rank,
books.title,
creators.name,
my_search.doc,
query
FROM (SELECT
      simple_search.id,
      simple_search.doc,
      query,
      ts_rank(simple_search.doc, query) rank
      FROM simple_search, to_tsquery('christmas & ca:*') AS query
      WHERE query @@ ranked_search.doc
      ORDER BY rank DESC
      LIMIT 50) my_search
LEFT JOIN books ON (books.id = my_search.id)
LEFT JOIN books_creators ON books_creators.book_id = books.id
LEFT JOIN creators ON books_creators.creator_id = creators.id
```

This should be enough to get started to build a search feature
with Postgres in English.

## When Not to Use Postgres Full Text Search

Postgres works great to get implement an MVP of search in a project.  However as the project grows, you may find that your users may need more than what Postgres FTS has to offer.  Here’s some signs that it may be time to switch to something else like [Elastic Search](https://www.elastic.co/), [Sphinx](http://sphinxsearch.com/), or [Algolia](https://www.algolia.com/).

If you need massive scale.  From the research I’ve done, [people](https://news.ycombinator.com/item?id=8383746) have had [success](https://news.ycombinator.com/item?id=8381855) with Postgres FTS with over 20 million documents.  But there may come a point where your application will outgrow what Postgres can offer.  At that point you may want to look for something else.

If you need support for multiple languages.  Postgres full text search only [supports a handful of languages well](https://stackoverflow.com/questions/39751892/get-full-list-of-full-text-search-configuration-languages).  From the research I’ve done, Elastic search/Solr supports multiple languages much better.

If you need fine grain control over the search ranking.  Postgres offers a few ranking algorithms out of the box, but if you need something outside of those, you may want to look elsewhere.  The Postgres documentation says you write your own ranking algorithms, however, they need to be written in C.  Other products offer more flexible ranking algorithms.

## What’s next

For my next post I plan on creating a web front end for the search results. I’ll show how to create a typeahead in 3 different front end languages using this dataset as a back end.  We'll look at React (JavaScript), Angular (TypeScript), and Reagent/Reframe (ClojureScript).


## Further Reading

- The official PostgreSQL documentation https://www.postgresql.org/docs/10/textsearch.html
- http://rachbelaid.com/postgres-full-text-search-is-good-enough/
- http://web.archive.org/web/20190418012835/http://www.shisaa.jp/postset/postgresql-full-text-search-part-1.html
- http://web.archive.org/web/20190417191152/http://shisaa.jp/postset/postgresql-full-text-search-part-2.html
- http://web.archive.org/web/20190418012731/http://shisaa.jp/postset/postgresql-full-text-search-part-3.html

