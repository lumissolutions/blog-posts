# Using OpenAPI to Accelerate Development

One of the engagements completed at CIL involved helping a
client connect a mobile app to their existing back end
services.  Due to security requirements, the app couldn't
directly connect to these services.  CIL developed a solution
that acted as a middle tier between the app and their existing
services.  It was written in TypeScript, hosted in Node.js and
deployed to AWS Lambda.  We took a design first approach to
building this system, then we implemented the base architecture
based on the design.  We introduced some tools to design, mock,
test and implement the new middle tier based using
[OpenAPIv3](http://spec.openapis.org/oas/v3.0.3) as the center
piece to the effort.

Before I started writing code on the project, I first started to
investigate what APIs from the existing system needed to be
exposed to the app.  Then I worked on designing a new API around
those existing APIs in OpenAPIv3.  Using OpenAPI opened up a lot
a new possibilities because of the tooling around it. In this
project its tooling was used to produce documentation, test and
validate the API, validate data, create a mock server and even
generate types.

## Documentation

There are many OpenAPI renderers.  I choose
[ReDoc](https://github.com/Redocly/redoc) for this project
because it generated documation that looked really good and it's
CLI tools enabled us to run an NPM command to get a local server up
and running to browse the documentation.

We didn't need the execute functionality in [Swagger
UI](https://swagger.io/tools/swagger-ui/).  The front end team
used this to know how to implement endpoints in the app.

## API Validation

There's almost no business logic in the new middle tier, so
there were no unit tests written.  However, we did create a full
suite of integration tests that tested the middle tier end to
end.  I used [tape](https://github.com/substack/tape) for
testing and [axios](https://github.com/axios/axios) as the HTTP
client.  I wrote at least one test around each end point to test
it.  The result being that every endpoint was tested.

One of the tools used on the project was
[Prism](https://github.com/stoplightio/prism). Prism has a
feature where you can use it to proxy requests to an HTTP API.
The proxy loads the OpenAPI specification then validates the
requests and responses against the specification.  This meant
that the test suite could be pointed to the proxy instead of
directly to the local server. This means that the suite itself
doesn't have to implement any validation logic - all of that is
handled by the proxy.  It is also a good way to ensure that the
specification and the implementation match.

## Data Validation

While Prism is a fantastic tool, it should not be used in
production to validate data.  It's meant to be used as a
development and testing tool.  The OpenAPI schema definitions
can still be used to validate data in production. To validate
data in production [Ajv](https://github.com/ajv-validator/ajv)
was used in the server.  There's a couple of catches though to
using this approach.  The first is that the schemas should be
defined in separates files (requests and responses) and
referenced in the main OpenAPI doc so they can be complied by
Ajv.  The second is that by default Ajv is configure by default
so load JSON schema defined by draft-07, but OpenAPI uses a subset of draft-04.
The schema definitions should be defined using draft-04.  This
is further discussed in [this
post](https://apisyouwonthate.com/blog/openapi-and-json-schema-divergence).
We needed to ensure that Ajv was configured to load schemas
defined using draft-04.

The good news is that OpenAPI v3.1 [will use JSON Schema draft
2019-09](https://apisyouwonthate.com/blog/openapi-v31-and-json-schema-2019-09).
The bad news is that OpenAPI v3.1 is not finalized yet.  And
even after it's finalized, we'll have to wait for the tooling to
catch up.

## Mock Server

Another thing that was very successful on this project was using
Prism as a tool to mock data on the server for endpoints that
were not implemented.  While the backend team was working on
implementing the endpoints, the front end team implemented the
endpoint in the app against the mock server.  Prism can be
configured to either return the examples defined in the schemas
or to dynamically generate data.  We used dynamically generated
data. This allows both teams to work in parallel so that the
backend team is not blocking the front end team.

We strictly used it as a local development tool.  However, in
the future it would be nice to deploy the mock server to AWS
Lambda.  There would need to be some work done to Prism to
support that environment though.

## Code Generation

Since the mobile app and the back end used TypeScript, we used a
tool to generate the TypeScript types from the schemas.  This
allowed the OpenAPI specification to be the source of truth for
even parts of the code.
[json-schema-to-typescript](https://github.com/bcherny/json-schema-to-typescript)
was used to do that.

This particular set up used three git repositories; mobile,
backend, specification.  The specification repository was
imported as a git submodule in the other repositories and the
auto generated types were committed in the repository instead of
ignored.  This meant that the people consuming the specification
repository didn't have to worry about generating the types.
However, the developer updating the specification had to remember to rebuild
the types after a commit.  I'm not 100% sold on this approach,
I'll see how it works out in practice over time.

## Other Possibilities

One area that would be interesting to investigate is using the
schema definitions to validate data in the app before it's sent
over to the server.  This would be that the JSON schemas would
need to be accessible from the server.  This could mean less
traffic to the server and reduced load.  Another possibility
would be to dynamically create a form from the schema
definition.

## Conclusion

Designing the API in OpenAPI really helped accelerate
development.  It allowed everyone to see what APIs were going to
be built, to validate the APIs as they were being built, for
both teams to work at the same time, and validate data as it was
deployed.  It created a shared context for everyone involved in
the project, from developers and infrastructure developers to
project management and QA.


# References

  * OpenAPI 3.0.3 Spec; http://spec.openapis.org/oas/v3.0.3
  * JSON Schema draft-04 spec; https://tools.ietf.org/html/draft-wright-json-schema-00
  * JSON Schema draft-04 validation spec; https://tools.ietf.org/html/draft-wright-json-schema-validation-00
  * Prism; https://github.com/stoplightio/prism
  * ReDoc; https://github.com/Redocly/redoc
