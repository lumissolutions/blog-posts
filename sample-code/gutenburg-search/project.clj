(defproject gutenburg-search "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [compojure "1.6.1"]
                 [ring/ring-defaults "0.3.2"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [org.postgresql/postgresql "42.2.6.jre7"]
                 [com.layerware/hugsql "0.4.9"]
                 [clj-time "0.15.2"]
                 [hiccup "2.0.0-alpha2"]
                 [ragtime "0.8.0"]]
  :plugins [[lein-ring "0.12.5"]]
  :ring {:handler gutenburg-search.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}})
