 (ns user
  (:require [ragtime.jdbc :as jdbc]
            [gutenburg-search.db :refer [db]]))

;; https://github.com/weavejester/ragtime/wiki/Getting-Started
(def config
  {:datastore  (jdbc/sql-database db)
   :migrations (jdbc/load-resources "migrations")})
