 (ns gutenburg-search.views.search
  (:use [hiccup.core :only (html h)]))

(defn layout [title & content]
  (html {:lang "en"}
         [:head
          [:title title]
          [:link {:rel "stylesheet" :href "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"}]
          [:body
           [:div {:class "container"} content ]]]))

(defn search [params results]
  (html
    [:div
     [:h2 "Search Project Gutenburg"]
     [:form {:action "/search"}
      [:div.form-group
       [:label "Search"]
       [:input.form-control {:id "search" :type "text" :placeholder "Search" :name "query" :value (get params "query") }] ]
      [:div.form-group
       [:label "Search Type"]
       [:select.form-control {:name "search_type" :value (get params "search_type" "simple")}
        [:option {:value "simple"} "Simple"]
        [:option {:value "to_tsvector"} "To Vector"]
        [:option {:value "weighted"} "Weighted"] ] ]
      [:button "Search"]]
     [:br]
     [:br]
     [:table.table
      [:tr
       [:th "Rank"]
       [:th "Id"]
       [:th "Title"]
       [:th "Author"]]
      (for [{:keys [id name title rank]} results]
           [:tr
            [:td (h rank)]
            [:td (h id)]
            [:td (h title)]
            [:td (h name)]])]]))

(defn view [params results]
  (layout "Search" (search params results)))
