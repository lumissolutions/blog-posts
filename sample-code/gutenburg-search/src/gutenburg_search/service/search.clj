 (ns gutenburg-search.service.search
     (:require
       [gutenburg-search.views.search :refer [view]]
       [gutenburg-search.db :refer [db]]
       [gutenburg-search.repo.search :as search-repo]))

(defn do-search [params]
  (let [query (get params "query")
        search-type (get params "search_type")]
    (if (clojure.string/blank? query)
        []
        (search-repo/search-simple db {:search query}))))

(defn search [req]
  (let [results (do-search (:query-params req))]
    (println req)
    (view (:query-params req) results)))
