(ns gutenburg-search.service.books
    (:require
      [clojure.xml :as xml]
      [clojure.pprint :refer [pprint]]
      [gutenburg-search.repo.books :refer [save-all-book-data]]
      [gutenburg-search.service.xml :refer [get-attr
                                            get-about
                                            first-content
                                            find-tag
                                            find-tags
                                            find-tag-first-content]]))

(def f (clojure.java.io/file "/Users/bill/Downloads/cache/epub"))

(defn- map-hasFormat [data]
  (let [file (first-content data)
        metadata (:content file)
        uri (get-about file)
        modified (find-tag-first-content metadata :dcterms:modified)
        extant (find-tag-first-content metadata :dcterms:extent)
        mime-type (-> metadata
                    (find-tag :dcterms:format)
                    :content
                    (find-tag :rdf:Description)
                    :content
                    (find-tag :rdf:value)
                    (first-content))]
    {:uri uri
     :modified modified
     :extant extant
     :mime_type mime-type}))

(defn- get-files [book-content]
  (map map-hasFormat
       (find-tags book-content :dcterms:hasFormat)))

(defn- get-creator [book-content]
  (let [creator (find-tag-first-content book-content :dcterms:creator)
        creator-content (:content creator) ]
    {:id (get-about creator)
     :name (find-tag-first-content creator-content :pgterms:name)
     :birth_date (find-tag-first-content creator-content :pgterms:birthdate)
     :death_date (find-tag-first-content creator-content :pgterms:deathdate)
     :webpage (find-tag-first-content creator-content :pgterms:webpage)
     :aliases (map #(first (:content %))
                   (find-tags creator-content :pgterms:alias))}))

(defn- get-subjects [book-content]
  (map
    (fn [tag]
        (let [desc-content (-> tag
                               :content
                               (find-tag :rdf:Description)
                               :content)]
          {:value (find-tag-first-content desc-content :rdf:value)
           :kind (-> desc-content
                     (find-tag :dcam:memberOf)
                     (get-attr :rdf:resource ))}))
    (find-tags book-content :dcterms:subject)))

(defn- get-language [content]
  (-> content
      (find-tag :dcterms:language)
      :content
      (find-tag :rdf:Description)
      :content
      (find-tag-first-content :rdf:value)))

(defn- get-book [data]
  (-> data
      :content
      (find-tag :pgterms:ebook)))

(defn- parse-book [data]
  (let [book (get-book data)
        book-data {:id (get-about book)
                   :files (get-files (:content book))
                   :title  (find-tag-first-content (:content book) :dcterms:title)
                   :alternative (find-tag-first-content (:content book) :dcterms:alternative)
                   :downloads  (find-tag-first-content (:content book) :pgterms:downloads)
                   :table_of_contents (find-tag-first-content (:content book) :dcterms:tableOfContents)
                   :issued (find-tag-first-content (:content book) :dcterms:issued)
                   :creator (get-creator (:content book))
                   :language (get-language (:content book))
                   :subjects (get-subjects (:content book))}]
    (pprint book-data)
    (save-all-book-data book-data)))

(defn- parse-file [file]
  (println "reading file" file)
  (let [data (xml/parse file)]
    (parse-book data)))

(defn import-index []
  (println "--- new request ---")
  (println "checking out this folder" f)
  (doseq [file (file-seq f)]
         (if (.isFile file)
             (parse-file file)
             (println "not file" file))))

