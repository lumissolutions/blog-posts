(ns gutenburg-search.service.xml)

(defn find-first
  [f coll]
  (first (filter f coll)))

(defn get-attr [tag attr]
  (get-in tag [:attrs attr]))

(defn get-about [tag]
  (get-attr tag :rdf:about))

(defn first-content [tag]
  (first (:content tag)))

(defn find-tags [content tag-name]
  (filter #(= (:tag %) tag-name) content))

(defn find-tag [content tag-name]
  (find-first #(= (:tag %) tag-name) content))

(defn find-tag-first-content [content tag-name]
  (first-content (find-tag content tag-name)))
