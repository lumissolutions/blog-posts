(ns gutenburg-search.repo.books
    (:require
      [clojure.java.jdbc :as jdbc]
      [clojure.pprint :refer [pprint]]
      [clj-time.core :as t]
      [gutenburg-search.db :refer [db]]))



(defn find-book [book]
  (first
    (jdbc/query db ["SELECT * FROM books WHERE id = ? LIMIT 1" (:id book)])) )

(defn insert-book! [book tcon]
  (jdbc/insert! tcon :books
                (-> book
                    (select-keys [:id
                                  :title
                                  :alternative
                                  :description
                                  :table_of_contents
                                  :issued
                                  :language
                                  :downloads])
                    (update-in [:issued]
                               #(try
                                  (java.sql.Date/valueOf %)
                                  (catch Exception e nil)))
                    (update-in [:downloads]
                               #(try
                                  (Integer/parseInt %)
                                  (catch Exception e 0))))))

(defn insert-creator! [creator book-id tcon]
  (when (:id creator)
    (let [previousCreator
          (first
            (jdbc/query db ["SELECT * FROM creators WHERE id = ? LIMIT 1" (:id creator)]))]
      (when (not previousCreator)
        (jdbc/insert! tcon :creators
                      (select-keys creator  [:id
                                             :name
                                             :birth_date
                                             :death_date])))

      (jdbc/insert! tcon :books_creators {:book_id book-id :creator_id (:id creator)}))))

(defn insert-files! [files book-id tcon]
  (jdbc/insert-multi! tcon
                      :files
                      (->> files
                           (filter #(:uri %))
                           (map (fn [file]
                                   (-> file
                                       (assoc :book_id book-id)
                                       (update-in [:modified] #(try
                                                                 (java.sql.Timestamp/from (java.time.Instant/parse (str % "Z")))
                                                                 (catch Exception e nil)))))))))

(defn find-subject [subject]
              (first
                (jdbc/query db ["SELECT * FROM subjects WHERE value = ? AND kind = ? LIMIT 1" (:value subject) (:kind subject)])))

(defn insert-subject! [subject tcon]
  (println "insert subject")
  (first
    (jdbc/insert! tcon :subjects subject)))

(defn find-or-insert-subject! [subject tcon]
  (println "find subject" subject)
  (let [previousSubject (find-subject subject)]
    (if previousSubject
        previousSubject
        (insert-subject! subject tcon))))

(defn insert-subjects! [subjects book-id tcon]
  (doseq [subject subjects]
         (let [saved-subject (find-or-insert-subject! subject tcon)]
           (pprint saved-subject)
           (jdbc/insert! tcon :books_subjects {:book_id book-id :subject_id (:id saved-subject)}))))

(defn save-all-book-data [book]
  (jdbc/with-db-transaction [t-con db]
    (let [saved-book (find-book book)]
      (if (not saved-book)
          (do (insert-book! book t-con)
              (insert-creator! (:creator book) (:id book) t-con)
              (insert-files! (:files book) (:id book) t-con)
              (insert-subjects! (:subjects book) (:id book) t-con))
          (println "book already saved - skipping")))))
