 (ns gutenburg-search.repo.search
  (:require
      [gutenburg-search.db :refer [db]]
      [hugsql.core :as hugsql]))

(hugsql/def-db-fns "gutenburg_search/repo/search.sql")
