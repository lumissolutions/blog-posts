-- :name search-simple :? :*
-- :doc
SELECT
my_search.id,
my_search.rank,
books.title,
creators.name,
my_search.doc,
query
FROM (SELECT
  simple_search.id,
  simple_search.doc,
  query,
  ts_rank(simple_search.doc, query) rank
  FROM simple_search, plainto_tsquery(:search) AS query
  WHERE query @@ simple_search.doc
  ORDER BY rank DESC
  LIMIT 50) my_search
LEFT JOIN books ON (books.id = my_search.id)
LEFT JOIN books_creators ON books_creators.book_id = books.id
LEFT JOIN creators ON books_creators.creator_id = creators.id

-- :name search-vector :? :*
-- :doc
SELECT
my_search.id,
my_search.rank,
books.title,
creators.name,
my_search.doc,
query
FROM (SELECT
  simple_search.id,
  simple_search.doc,
  query,
  ts_rank(simple_search.doc, query) rank
  FROM simple_search, to_tsquery(:search) AS query
  WHERE query @@ simple_search.doc
  ORDER BY rank DESC
  LIMIT 50) my_search
LEFT JOIN books ON (books.id = my_search.id)
LEFT JOIN books_creators ON books_creators.book_id = books.id
LEFT JOIN creators ON books_creators.creator_id = creators.id
