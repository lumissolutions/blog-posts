(ns gutenburg-search.handler
  (:require
    [clj-time.jdbc]
    [compojure.core :refer :all]
    [compojure.route :as route]
    [gutenburg-search.service.search :refer [search]]
    [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
    [gutenburg-search.service.books :refer [import-index]]))

(defroutes app-routes
  (GET "/" [] "Hello World")
  (GET "/search" [] search)
  (GET "/import" [] (fn [req]
                        (import-index)
                        "Success!"))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))
