CREATE TABLE creators(
  id TEXT,
  name TEXT,
  webpage TEXT,
  aliases TEXT[],
  death_date TEXT,
  birth_date TEXT,
  PRIMARY KEY(id)
);
