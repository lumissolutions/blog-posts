CREATE TABLE files(
  extant TEXT,
  mime_type TEXT,
  uri TEXT,
  modified TIMESTAMP,
  book_id TEXT REFERENCES books(id),
  PRIMARY KEY(uri)
);
