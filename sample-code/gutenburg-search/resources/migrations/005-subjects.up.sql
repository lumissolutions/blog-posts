CREATE TABLE subjects(
  id SERIAL,
  value TEXT,
  kind TEXT,
  PRIMARY KEY(id)
);
