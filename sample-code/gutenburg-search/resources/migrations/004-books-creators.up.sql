CREATE TABLE books_creators (
  book_id TEXT REFERENCES books(id),
  creator_id TEXT REFERENCES creators(id),
  PRIMARY KEY (book_id, creator_id)
);
