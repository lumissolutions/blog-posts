CREATE TABLE books(
  id TEXT,
  title TEXT,
  alternative TEXT,
  description TEXT,
  language TEXT,
  table_of_contents TEXT,
  issued DATE,
  downloads INT,
  metadata JSONB,
  PRIMARY KEY (id)
);
