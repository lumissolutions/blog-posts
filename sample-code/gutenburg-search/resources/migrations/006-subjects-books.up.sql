CREATE TABLE books_subjects (
  book_id TEXT REFERENCES books(id),
  subject_id INT REFERENCES subjects(id),
  PRIMARY KEY (book_id, subject_id)
);
